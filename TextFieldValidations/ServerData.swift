//
//  ServerData.swift
//  TextFieldValidations
//
//  Created by ok on 9/14/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import Alamofire
import FTIndicator
import Foundation

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class ServerData: NSObject {

    // MARK: GETFROMSERVER
    class func getResponseFromURL(urlString: String,  completion: @escaping (_ success: [String : AnyObject]) -> Void){
        if Connectivity.isConnectedToInternet {
            FTIndicator.showProgress(withMessage: DefinedStrings.loaderText)
            Alamofire.request(urlString).responseJSON { response in
                FTIndicator.dismissProgress()
                switch response.result {
                case .success:
                    let json = response.result.value
                    completion(json as! [String : AnyObject])
                case .failure(let error):
                    print(error)
                    FTIndicator.showError(withMessage: DefinedStrings.serverError)
                }
            }
        }else{
            FTIndicator.showError(withMessage: DefinedStrings.internetError)
        }
    }
    
    // MARK: POSTTOSERVER
    class func postResponseFromURL(urlString: String, params : [String : String],  completion: @escaping (_ success: [String : AnyObject]) -> Void){
        if Connectivity.isConnectedToInternet {
            FTIndicator.showProgress(withMessage: DefinedStrings.loaderText)
            Alamofire.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: [:]).responseJSON { (response:DataResponse<Any>) in
                FTIndicator.dismissProgress()
                switch(response.result) {
                case .success(_):
                        let json = response.result.value
                        completion(json as! [String : AnyObject])
                    break
                case .failure(_):
                    FTIndicator.showError(withMessage: DefinedStrings.serverError)
                    break
                }
            }
        }else{
            FTIndicator.showError(withMessage: DefinedStrings.internetError)
        }
    }
    
    
    // MARK: UPLOADTOSERVER
    class func uploadTaskFor(URLString : String , parameters : [String : String] , filesArray : NSArray,  completion: @escaping (_ success: [String : AnyObject]) -> Void){
        
        
        if Connectivity.isConnectedToInternet{

        
        let imgData = UIImageJPEGRepresentation(UIImage(named: "1.png")!,1)
        
        
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                MultipartFormData.append(UIImageJPEGRepresentation(UIImage(named: "1.png")!, 1)!, withName: "photos[1]", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                MultipartFormData.append(UIImageJPEGRepresentation(UIImage(named: "1.png")!, 1)!, withName: "photos[2]", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                
                
        }, to: URLString) { (result) in
            FTIndicator.dismissProgress()
            switch(result) {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let json = response.result.value
                    completion(json as! [String : AnyObject])
                }
                break;
            case .failure(_):
                FTIndicator.showError(withMessage: DefinedStrings.serverError)
                break;
            }
        }
        }else{
            FTIndicator.showError(withMessage: DefinedStrings.internetError)
        }
        
    }
    
    
 
}
