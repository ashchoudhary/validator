//
//  GlobalContants.swift
//  TextFieldValidations
//
//  Created by ok on 9/14/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit

struct URLConstants  {
    static let baseUrl = "http://www.elvektechnologies.com/digilab2/"
    static let allTests = "tests/getAllTest2"
}


struct DefinedStrings  {
    static let internetError = "Internet Connectivity is Not Available"
    static let serverError = "Server Not Responding"
    static let loaderText = "Loading..."
}



