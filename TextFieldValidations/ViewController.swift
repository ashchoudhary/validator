//
//  ViewController.swift
//  TextFieldValidations
//
//  Created by ok on 9/14/17.
//  Copyright © 2017 Praveen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftValidator

class ViewController: UIViewController  {

    let validator = Validator()

    
    @IBOutlet var firstNameTXT : UITextField!
    @IBOutlet var emailTXT : UITextField!
    @IBOutlet var passwordTXT : UITextField!
    @IBOutlet var phoneTXT : UITextField!
    @IBOutlet var pinTXT : UITextField!
    @IBOutlet var textter : UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        validator.validate(delegate: self as! ValidationDelegate)
        validator.registerField(textField: firstNameTXT, rules: [RequiredRule(), FullNameRule()])

        
        
        
        self.textter.text = ""
        
        //callApi()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func callApi(){
        let strgURL : String  = URLConstants.baseUrl + URLConstants.allTests
        ServerData.getResponseFromURL(urlString: strgURL, completion: { response in
            print(response)
            self.textter.text = String(describing : response)
        })
    }
    @IBAction func signupTapped(sender: AnyObject) {
        validator.validateField(textField: firstNameTXT) { (err) in
            print(err)
            
            firstNameTXT.layer.borderColor = UIColor.red.cgColor
            firstNameTXT.layer.borderWidth = 1.0

            
        }
    }

}

